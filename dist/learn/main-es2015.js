(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navhead></app-navhead>\n\n\n\n\n\n\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/begin/begin.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/begin/begin.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>begin works!</p>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/htmlresources/htmlresources.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/htmlresources/htmlresources.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-expansion-panel>\n    <mat-expansion-panel-header>\n       <mat-panel-title>\n          Personal data\n       </mat-panel-title>\n       <mat-panel-description>\n          Type name and age\n       </mat-panel-description>\n    </mat-expansion-panel-header>\n    <mat-form-field>\n       <input matInput placeholder=\"Name\">\n    </mat-form-field>\n    <mat-form-field>\n       <input matInput placeholder=\"Age\">\n    </mat-form-field>\n </mat-expansion-panel>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/htmls/htmls.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/htmls/htmls.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\" style=\"display:block; position:relative;margin-top:2em;\">\n  <div class=\"card-body\">\n    <mat-expansion-panel>\n      <mat-expansion-panel-header>\n        <mat-panel-title>\n          Create two elements seperated\n        </mat-panel-title>\n        <mat-panel-description (click)=\"isShow = !isShow\">\n          Click to show test box\n        </mat-panel-description>\n      </mat-expansion-panel-header>\n\n      <mat-form-field>\n        <div class=\"input-group flex-nowrap\">\n          <div class=\"input-group-prepend\">\n            <span class=\"input-group-text\" id=\"addon-wrapping\">Padding</span>\n          </div>\n          <input\n            type=\"text\"\n            class=\"form-control\"\n            [(ngModel)]=\"padding\"\n            placeholder=\"33\"\n            aria-label=\"Username\"\n            aria-describedby=\"addon-wrapping\"\n          />\n        </div>\n      </mat-form-field>\n\n      <mat-form-field>\n        <div class=\"input-group flex-nowrap\">\n          <div class=\"input-group-prepend\">\n            <span class=\"input-group-text\" id=\"addon-wrapping\">Margin</span>\n          </div>\n          <input\n            type=\"text\"\n            class=\"form-control\"\n            [(ngModel)]=\"margin\"\n            placeholder=\"33\"\n            aria-label=\"Username\"\n            aria-describedby=\"addon-wrapping\"\n          />\n        </div>\n      </mat-form-field>\n      <br />\n\n      <mat-form-field>\n        <div class=\"input-group flex-nowrap\">\n          <div class=\"input-group-prepend\">\n            <span class=\"input-group-text\" id=\"addon-wrapping\">Width %</span>\n          </div>\n          <input\n            type=\"text\"\n            class=\"form-control\"\n            [(ngModel)]=\"width\"\n            placeholder=\"33\"\n            aria-label=\"Username\"\n            aria-describedby=\"addon-wrapping\"\n          />\n        </div>\n      </mat-form-field>\n\n      <mat-form-field>\n        <div class=\"input-group flex-nowrap\">\n          <div class=\"input-group-prepend\">\n            <span class=\"input-group-text\" id=\"addon-wrapping\">Height%</span>\n          </div>\n          <input\n            type=\"text\"\n            class=\"form-control\"\n            [(ngModel)]=\"height\"\n            placeholder=\"33\"\n            aria-label=\"Username\"\n            aria-describedby=\"addon-wrapping\"\n          />\n        </div>\n      </mat-form-field>\n      <br />\n    </mat-expansion-panel>\n    <br />\n\n    <div class=\"ruleOptionsPanel\" *ngIf=\"!isShow\">\n      <div style=\"border: 1px solid rgba(0,0,0,.125);\">\n        <div\n          class=\"card-body\"\n          [ngStyle]=\"{\n            'background-color': 'tomato',\n            border: '1px solid rgba(0,0,0,.125)',\n            'text-align':'center'\n          }\"\n          [style.width.%]=\"this.width\"\n          [style.height.%]=\"this.height\"\n          [style.padding.px]=\"this.padding\"\n          [style.margin.px]=\"this.margin\"\n        >\n          Button\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/navhead/navhead.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/navhead/navhead.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"sidenav-container\">\n  <mat-sidenav\n    #drawer\n    class=\"sidenav\"\n    fixedInViewport\n    [attr.role]=\"(isHandset$ | async) ? 'dialog' : 'navigation'\"\n    [mode]=\"(isHandset$ | async) ? 'over' : 'side'\"\n    [opened]=\"(isHandset$ | async) === false\"\n  >\n    <mat-toolbar>Menu</mat-toolbar>\n    <mat-nav-list>\n      <a mat-list-item href=\"#\">HTML/CSS</a>\n      <a mat-list-item href=\"#\">JS</a>\n      <a mat-list-item href=\"#\">PHP</a>\n    </mat-nav-list>\n  </mat-sidenav>\n  <mat-sidenav-content>\n    <mat-toolbar color=\"primary\">\n      <button\n        type=\"button\"\n        aria-label=\"Toggle sidenav\"\n        mat-icon-button\n        (click)=\"drawer.toggle()\"\n        *ngIf=\"isHandset$ | async\"\n      >\n        <mat-icon aria-label=\"Side nav toggle icon\">menu</mat-icon>\n      </button>\n      <span>learn</span>\n      <br />\n      <hr />\n    </mat-toolbar>\n    <!-- Add Content Here -->\n\n    <mat-toolbar class=\"mx-auto\" style=\"display: block;width: 100%;\">\n      <mat-tab-group class=\"mx-auto\" style=\"width: 100%;\">\n        <mat-tab label=\"BUTTON\">\n          <mat-grid-list cols=\"4\" rowHeight=\"100px\">\n            <mat-grid-tile [colspan]=\"4\" [rowspan]=\"7\">\n              <app-htmls></app-htmls>\n            </mat-grid-tile>\n          </mat-grid-list>\n        </mat-tab>\n        <mat-tab label=\"RESOURCES\">\n          <mat-grid-list cols=\"4\" rowHeight=\"100px\">\n            <mat-grid-tile [colspan]=\"4\" [rowspan]=\"7\">\n                <app-htmlresources></app-htmlresources>\n            </mat-grid-tile>\n          </mat-grid-list>\n        </mat-tab>\n        <mat-tab label=\"PRACTICE\"> Content 3 </mat-tab>\n      </mat-tab-group>\n    </mat-toolbar>\n  </mat-sidenav-content>\n</mat-sidenav-container>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/sidenav/sidenav.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/sidenav/sidenav.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>sidenav works!</p>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/twobtn/twobtn.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/twobtn/twobtn.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\" style=\"display:block; position:relative;margin-top:2em;\">\n    <div class=\"card-body\">\n      <mat-expansion-panel>\n        <mat-expansion-panel-header>\n          <mat-panel-title>\n            Create two elements seperated\n          </mat-panel-title>\n          <mat-panel-description (click)=\"isShow = !isShow\">\n            Click to show test box\n          </mat-panel-description>\n        </mat-expansion-panel-header>\n\n        <mat-form-field>\n          <div class=\"input-group flex-nowrap\">\n            <div class=\"input-group-prepend\">\n              <span class=\"input-group-text\" id=\"addon-wrapping\">Padding</span>\n            </div>\n            <input\n              type=\"text\"\n              class=\"form-control\"\n              [(ngModel)]=\"padding\"\n              placeholder=\"33\"\n              aria-label=\"Username\"\n              aria-describedby=\"addon-wrapping\"\n            />\n          </div>\n        </mat-form-field>\n\n        <mat-form-field>\n          <div class=\"input-group flex-nowrap\">\n            <div class=\"input-group-prepend\">\n              <span class=\"input-group-text\" id=\"addon-wrapping\">Margin</span>\n            </div>\n            <input\n              type=\"text\"\n              class=\"form-control\"\n              [(ngModel)]=\"margin\"\n              placeholder=\"33\"\n              aria-label=\"Username\"\n              aria-describedby=\"addon-wrapping\"\n            />\n          </div>\n        </mat-form-field>\n        <br />\n\n        <mat-form-field>\n          <div class=\"input-group flex-nowrap\">\n            <div class=\"input-group-prepend\">\n              <span class=\"input-group-text\" id=\"addon-wrapping\">Width %</span>\n            </div>\n            <input\n              type=\"text\"\n              class=\"form-control\"\n              [(ngModel)]=\"width\"\n              placeholder=\"33\"\n              aria-label=\"Username\"\n              aria-describedby=\"addon-wrapping\"\n            />\n          </div>\n        </mat-form-field>\n\n        <mat-form-field>\n          <div class=\"input-group flex-nowrap\">\n            <div class=\"input-group-prepend\">\n              <span class=\"input-group-text\" id=\"addon-wrapping\">Height%</span>\n            </div>\n            <input\n              type=\"text\"\n              class=\"form-control\"\n              [(ngModel)]=\"height\"\n              placeholder=\"33\"\n              aria-label=\"Username\"\n              aria-describedby=\"addon-wrapping\"\n            />\n          </div>\n        </mat-form-field>\n        <br />\n      </mat-expansion-panel>\n      <br />\n\n      <div class=\"ruleOptionsPanel\" *ngIf=\"!isShow\">\n        <div style=\"border: 1px solid rgba(0,0,0,.125);\">\n          <div\n            class=\"card-body\"\n            [ngStyle]=\"{\n              'background-color': 'tomato',\n              border: '1px solid rgba(0,0,0,.125)',\n              'text-align':'center'\n            }\"\n            [style.width.%]=\"this.width\"\n            [style.height.%]=\"this.height\"\n            [style.padding.px]=\"this.padding\"\n            [style.margin.px]=\"this.margin\"\n          >\n            Button\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".tp-container {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  background: #eee;\n}\n.tp-section {\n  display: flex;\n  align-content: center;\n  align-items: center;\n  height: 60px;\n  width:100px;\n}\n.filler {\n  flex: 1 1 auto;\n}\n.gap {\n  margin-right: 10px;\n}\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLFNBQVM7RUFDVCxPQUFPO0VBQ1AsUUFBUTtFQUNSLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLFdBQVc7QUFDYjtBQUNBO0VBQ0UsY0FBYztBQUNoQjtBQUNBO0VBQ0Usa0JBQWtCO0FBQ3BCIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudHAtY29udGFpbmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG59XG4udHAtc2VjdGlvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgaGVpZ2h0OiA2MHB4O1xuICB3aWR0aDoxMDBweDtcbn1cbi5maWxsZXIge1xuICBmbGV4OiAxIDEgYXV0bztcbn1cbi5nYXAge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbiJdfQ== */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'learn';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _navhead_navhead_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./navhead/navhead.component */ "./src/app/navhead/navhead.component.ts");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm2015/layout.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm2015/button.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm2015/icon.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm2015/list.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm2015/grid-list.js");
/* harmony import */ var _sidenav_sidenav_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./sidenav/sidenav.component */ "./src/app/sidenav/sidenav.component.ts");
/* harmony import */ var _begin_begin_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./begin/begin.component */ "./src/app/begin/begin.component.ts");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm2015/tabs.js");
/* harmony import */ var _htmls_htmls_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./htmls/htmls.component */ "./src/app/htmls/htmls.component.ts");
/* harmony import */ var _twobtn_twobtn_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./twobtn/twobtn.component */ "./src/app/twobtn/twobtn.component.ts");
/* harmony import */ var _htmlresources_htmlresources_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./htmlresources/htmlresources.component */ "./src/app/htmlresources/htmlresources.component.ts");























const appRoutes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"] },
    { path: 'structure', component: _begin_begin_component__WEBPACK_IMPORTED_MODULE_16__["BeginComponent"] },
];
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            _navhead_navhead_component__WEBPACK_IMPORTED_MODULE_9__["NavheadComponent"],
            _sidenav_sidenav_component__WEBPACK_IMPORTED_MODULE_15__["SidenavComponent"],
            _begin_begin_component__WEBPACK_IMPORTED_MODULE_16__["BeginComponent"],
            _htmls_htmls_component__WEBPACK_IMPORTED_MODULE_18__["HtmlsComponent"],
            _twobtn_twobtn_component__WEBPACK_IMPORTED_MODULE_19__["TwobtnComponent"],
            _htmlresources_htmlresources_component__WEBPACK_IMPORTED_MODULE_20__["HtmlresourcesComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSidenavModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatToolbarModule"],
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_10__["LayoutModule"],
            _angular_material_button__WEBPACK_IMPORTED_MODULE_11__["MatButtonModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_12__["MatIconModule"],
            _angular_material_list__WEBPACK_IMPORTED_MODULE_13__["MatListModule"],
            _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_14__["MatGridListModule"],
            _angular_material_tabs__WEBPACK_IMPORTED_MODULE_17__["MatTabsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatExpansionModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatInputModule"]
        ],
        exports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/begin/begin.component.css":
/*!*******************************************!*\
  !*** ./src/app/begin/begin.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JlZ2luL2JlZ2luLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/begin/begin.component.ts":
/*!******************************************!*\
  !*** ./src/app/begin/begin.component.ts ***!
  \******************************************/
/*! exports provided: BeginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeginComponent", function() { return BeginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let BeginComponent = class BeginComponent {
    constructor() { }
    ngOnInit() {
    }
};
BeginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-begin',
        template: __webpack_require__(/*! raw-loader!./begin.component.html */ "./node_modules/raw-loader/index.js!./src/app/begin/begin.component.html"),
        styles: [__webpack_require__(/*! ./begin.component.css */ "./src/app/begin/begin.component.css")]
    })
], BeginComponent);



/***/ }),

/***/ "./src/app/htmlresources/htmlresources.component.css":
/*!***********************************************************!*\
  !*** ./src/app/htmlresources/htmlresources.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2h0bWxyZXNvdXJjZXMvaHRtbHJlc291cmNlcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/htmlresources/htmlresources.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/htmlresources/htmlresources.component.ts ***!
  \**********************************************************/
/*! exports provided: HtmlresourcesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HtmlresourcesComponent", function() { return HtmlresourcesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HtmlresourcesComponent = class HtmlresourcesComponent {
    constructor() { }
    ngOnInit() {
    }
};
HtmlresourcesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-htmlresources',
        template: __webpack_require__(/*! raw-loader!./htmlresources.component.html */ "./node_modules/raw-loader/index.js!./src/app/htmlresources/htmlresources.component.html"),
        styles: [__webpack_require__(/*! ./htmlresources.component.css */ "./src/app/htmlresources/htmlresources.component.css")]
    })
], HtmlresourcesComponent);



/***/ }),

/***/ "./src/app/htmls/htmls.component.css":
/*!*******************************************!*\
  !*** ./src/app/htmls/htmls.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".test\n{\n  background: tomato;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaHRtbHMvaHRtbHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7RUFFRSxrQkFBa0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9odG1scy9odG1scy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRlc3RcbntcbiAgYmFja2dyb3VuZDogdG9tYXRvO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/htmls/htmls.component.ts":
/*!******************************************!*\
  !*** ./src/app/htmls/htmls.component.ts ***!
  \******************************************/
/*! exports provided: HtmlsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HtmlsComponent", function() { return HtmlsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HtmlsComponent = class HtmlsComponent {
    constructor() {
        this.name = 'Angular';
        this.isImportant = 3;
        this.messages = [
            {
                id: 11214544,
                text: "<Message 11214544></Message>"
            },
            {
                id: 11214545,
                text: "Message 11214545"
            },
            {
                id: 11214546,
                text: "Message 11214546"
            },
            {
                id: 11214547,
                text: "Message 11214547"
            }
        ];
    }
    ngOnInit() {
    }
};
HtmlsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-htmls',
        template: __webpack_require__(/*! raw-loader!./htmls.component.html */ "./node_modules/raw-loader/index.js!./src/app/htmls/htmls.component.html"),
        styles: [__webpack_require__(/*! ./htmls.component.css */ "./src/app/htmls/htmls.component.css")]
    })
], HtmlsComponent);



/***/ }),

/***/ "./src/app/navhead/navhead.component.css":
/*!***********************************************!*\
  !*** ./src/app/navhead/navhead.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidenav-container {\n  height: 100%;\n}\n\n.sidenav {\n  width: 200px;\n}\n\n.sidenav .mat-toolbar {\n  background: inherit;\n}\n\n.mat-toolbar.mat-primary {\n  position: -webkit-sticky;\n  position: sticky;\n  top: 0;\n  z-index: 1;\n}\n\nmat-grid-tile {\n  background: lightblue;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmF2aGVhZC9uYXZoZWFkLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSx3QkFBZ0I7RUFBaEIsZ0JBQWdCO0VBQ2hCLE1BQU07RUFDTixVQUFVO0FBQ1o7O0FBRUE7RUFDRSxxQkFBcUI7QUFDdkIiLCJmaWxlIjoic3JjL2FwcC9uYXZoZWFkL25hdmhlYWQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zaWRlbmF2LWNvbnRhaW5lciB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLnNpZGVuYXYge1xuICB3aWR0aDogMjAwcHg7XG59XG5cbi5zaWRlbmF2IC5tYXQtdG9vbGJhciB7XG4gIGJhY2tncm91bmQ6IGluaGVyaXQ7XG59XG5cbi5tYXQtdG9vbGJhci5tYXQtcHJpbWFyeSB7XG4gIHBvc2l0aW9uOiBzdGlja3k7XG4gIHRvcDogMDtcbiAgei1pbmRleDogMTtcbn1cblxubWF0LWdyaWQtdGlsZSB7XG4gIGJhY2tncm91bmQ6IGxpZ2h0Ymx1ZTtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/navhead/navhead.component.ts":
/*!**********************************************!*\
  !*** ./src/app/navhead/navhead.component.ts ***!
  \**********************************************/
/*! exports provided: NavheadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavheadComponent", function() { return NavheadComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm2015/layout.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");




let NavheadComponent = class NavheadComponent {
    constructor(breakpointObserver) {
        this.breakpointObserver = breakpointObserver;
        this.name = 'Angular';
        this.isImportant = 3;
        this.messages = [
            {
                id: 11214544,
                text: "<Message 11214544></Message>"
            },
            {
                id: 11214545,
                text: "Message 11214545"
            },
            {
                id: 11214546,
                text: "Message 11214546"
            },
            {
                id: 11214547,
                text: "Message 11214547"
            }
        ];
        this.isHandset$ = this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["Breakpoints"].Handset)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(result => result.matches), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["shareReplay"])());
    }
};
NavheadComponent.ctorParameters = () => [
    { type: _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["BreakpointObserver"] }
];
NavheadComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-navhead',
        template: __webpack_require__(/*! raw-loader!./navhead.component.html */ "./node_modules/raw-loader/index.js!./src/app/navhead/navhead.component.html"),
        styles: [__webpack_require__(/*! ./navhead.component.css */ "./src/app/navhead/navhead.component.css")]
    })
], NavheadComponent);



/***/ }),

/***/ "./src/app/sidenav/sidenav.component.css":
/*!***********************************************!*\
  !*** ./src/app/sidenav/sidenav.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NpZGVuYXYvc2lkZW5hdi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/sidenav/sidenav.component.ts":
/*!**********************************************!*\
  !*** ./src/app/sidenav/sidenav.component.ts ***!
  \**********************************************/
/*! exports provided: SidenavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidenavComponent", function() { return SidenavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SidenavComponent = class SidenavComponent {
    constructor() { }
    ngOnInit() {
    }
};
SidenavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sidenav',
        template: __webpack_require__(/*! raw-loader!./sidenav.component.html */ "./node_modules/raw-loader/index.js!./src/app/sidenav/sidenav.component.html"),
        styles: [__webpack_require__(/*! ./sidenav.component.css */ "./src/app/sidenav/sidenav.component.css")]
    })
], SidenavComponent);



/***/ }),

/***/ "./src/app/twobtn/twobtn.component.css":
/*!*********************************************!*\
  !*** ./src/app/twobtn/twobtn.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3R3b2J0bi90d29idG4uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/twobtn/twobtn.component.ts":
/*!********************************************!*\
  !*** ./src/app/twobtn/twobtn.component.ts ***!
  \********************************************/
/*! exports provided: TwobtnComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TwobtnComponent", function() { return TwobtnComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TwobtnComponent = class TwobtnComponent {
    constructor() { }
    ngOnInit() {
    }
};
TwobtnComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-twobtn',
        template: __webpack_require__(/*! raw-loader!./twobtn.component.html */ "./node_modules/raw-loader/index.js!./src/app/twobtn/twobtn.component.html"),
        styles: [__webpack_require__(/*! ./twobtn.component.css */ "./src/app/twobtn/twobtn.component.css")]
    })
], TwobtnComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/g/Documents/le/learn/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map