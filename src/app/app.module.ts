import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatToolbarModule} from '@angular/material';
import { NavheadComponent } from './navhead/navhead.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { SidenavComponent } from './sidenav/sidenav.component';
import { BeginComponent } from './begin/begin.component';
import { MatTabsModule } from '@angular/material/tabs';
import { HtmlsComponent } from './htmls/htmls.component';
import { MatExpansionModule, MatInputModule } from '@angular/material';
import { TwobtnComponent } from './twobtn/twobtn.component';
import { HtmlresourcesComponent } from './htmlresources/htmlresources.component';





const appRoutes: Routes = [
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: AppComponent },
  { path: 'structure', component: BeginComponent },

];

@NgModule({
  declarations: [
    AppComponent,
    NavheadComponent,
    SidenavComponent,
    BeginComponent,
    HtmlsComponent,
    TwobtnComponent,
    HtmlresourcesComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    LayoutModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatTabsModule,
    MatExpansionModule,
    MatInputModule
  ],
  exports: [
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
