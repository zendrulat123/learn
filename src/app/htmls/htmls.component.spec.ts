import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlsComponent } from './htmls.component';

describe('HtmlsComponent', () => {
  let component: HtmlsComponent;
  let fixture: ComponentFixture<HtmlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
