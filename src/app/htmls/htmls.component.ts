import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-htmls',
  templateUrl: './htmls.component.html',
  styleUrls: ['./htmls.component.css']
})


export class HtmlsComponent implements OnInit {
  name = 'Angular';
  isImportant = 3;
  padding: string;
  margin: string;
  height: string;
  width: string;
  isShow: true;
  messages = [
    {
      id: 11214544,
      text: "<Message 11214544></Message>"
    },
    {
      id: 11214545,
      text: "Message 11214545"
    },
    {
      id: 11214546,
      text: "Message 11214546"
    },
    {
      id: 11214547,
      text: "Message 11214547"
    }

  ];
  constructor() {

   }

  ngOnInit() {

  }

}
