import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlresourcesComponent } from './htmlresources.component';

describe('HtmlresourcesComponent', () => {
  let component: HtmlresourcesComponent;
  let fixture: ComponentFixture<HtmlresourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlresourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlresourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
