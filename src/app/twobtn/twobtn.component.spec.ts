import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwobtnComponent } from './twobtn.component';

describe('TwobtnComponent', () => {
  let component: TwobtnComponent;
  let fixture: ComponentFixture<TwobtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwobtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwobtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
