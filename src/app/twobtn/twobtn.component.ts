import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-twobtn',
  templateUrl: './twobtn.component.html',
  styleUrls: ['./twobtn.component.css']
})
export class TwobtnComponent implements OnInit {
  public padding: string;
  public margin: string;
  public height: string;
  public width: string;
  isShow: true;

  constructor() { }

  ngOnInit() {
  }

}
