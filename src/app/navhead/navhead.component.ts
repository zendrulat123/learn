import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-navhead',
  templateUrl: './navhead.component.html',
  styleUrls: ['./navhead.component.css']
})
export class NavheadComponent {
  name = 'Angular';
  isImportant = 3;
  padding: string;
  margin: string;
  width: string;
  isShow: true;
  messages = [
    {
      id: 11214544,
      text: "<Message 11214544></Message>"
    },
    {
      id: 11214545,
      text: "Message 11214545"
    },
    {
      id: 11214546,
      text: "Message 11214546"
    },
    {
      id: 11214547,
      text: "Message 11214547"
    }

  ];
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver) {}

}
